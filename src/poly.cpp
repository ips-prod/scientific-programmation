#include "../headers/basis.h"
#include <iostream>

Poly::Poly()
{
  arma::mat H;
  arma::mat L;
}

Poly::~Poly(){}


arma::rowvec Poly::evalHermite(int n, arma::rowvec zVals)
{
  int i;

  if (n==0)
  {
    return arma::ones(zVals.n_elem).t();
    
  }else if (n==1)
  {
    return 2*zVals;
    
  }else
  {
    arma::rowvec H0 = evalHermite(0, zVals);
    arma::rowvec H1 = evalHermite(1, zVals);
    arma::rowvec temp;
    
    for (i=1;i<n;i++)
    {
      temp = (2*zVals)%H1 - 2*i*H0;
      H1 = temp;
      H0 = H1;
    }
    return H1;
  }
}

void Poly::calcHermite(int N, arma::vec zVals)
{
    int i;

    this->H = arma::mat(zVals.n_elem, N+1);

    if (N == 0)
    {
        this->H.col(0) = arma::ones(zVals.n_elem);

    }else if (N == 1){
        this->H.col(0) = arma::ones(zVals.n_elem);
        this->H.col(1) = 2*zVals;
    }else if (N > 1){
        this->H.col(0) = arma::ones(zVals.n_elem);
        this->H.col(1) = 2*zVals;
        
        for (i=2;i<=N;i++)
        this->H.col(i) = (2*zVals)%this->H.col(i-1) - 2*(i-1)*this->H.col(i-2);
    }else{
        throw std::invalid_argument("N must be positive");
    }
}

void Poly::calcLaguerre(int m, int N, arma::vec zVals)
{
    int i,j;
    int size=zVals.n_elem;
    this->L = arma::cube(size, N+1, m+1);

    for (j = 0 ; j <= m ; j++)
    {
        if (N == 0)
        {
            this->L.slice(j).col(0)=arma::ones(size);     
        }
        else if (N == 1)
        {
            this->L.slice(j).col(0)=arma::ones(size);
            this->L.slice(j).col(1)=(1+j)-zVals;
            
        }
        else
        {
            this->L.slice(j).col(0)=arma::ones(size);
            this->L.slice(j).col(1)=(1+j)-zVals;
            
            for (i = 2 ; i <= N ; i++)
            {
                this->L.slice(j).col(i) = (2+(j-1-zVals)/(double)i)%this->L.slice(j).col(i-1)- (1+((j-1)/(double)i))*this->L.slice(j).col(i-2);
            }
        }  
    }
}

arma::vec Poly::hermite(int n) {
    return this->H.col(n);
}

arma::vec Poly::laguerre(int m, int n){
  arma::mat mat_M=this->L.slice(m); 
    return mat_M.col(n);
}

