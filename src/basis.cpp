#include "../headers/basis.h"
#include "../headers/facto.h"
#include "../headers/poly.h"
#include <cmath>     
#include <stdexcept>

Basis::Basis()
{

}

Basis::Basis(double br, double bz, int N, double Q)  
{
    this->br_ = br;
    this->bz_ = bz;
    this->N_= N;
    this->Q_= Q;

 	auto n_z_max = [N, Q]( int i) {
        return ((double) N + 2.0) * pow(Q, 2.0/3.0) + 0.5 - i * Q;
    };

    for (this->mMax = 0; n_z_max(this->mMax + 1) >= 1.0; this->mMax++)
        ;

    // n_max
    this->nMax = arma::ivec(this->mMax);
    for ( int m = 0; m < this->mMax; m++) {
        this->nMax(m) = (arma::sword)(
                            0.5 * ((double) (this->mMax - m - 1)) + 1.0);
    }

    //  n_zMax
    this->n_zMax = arma::imat(this->mMax, this->nMax(0));
    this->n_zMax.fill(0);
    for ( int m = 0; m < this->mMax; m++) {
        for ( int n = 0; n < this->nMax(m); n++) {
            this->n_zMax(m, n) = n_z_max(m + 2 * n + 1);
        }
    }
}



double Basis::calc_nzMax(int N, double Q, int i)
{
    return (N + 2) * pow(Q, 2.0/3.0) + 0.5 - i * Q;
}

arma::vec Basis::zPart(arma::vec z, int n_z)
{
	Poly poly;
    poly.calcHermite(n_z, z / this->bz_);
	double A;
	double B;
	arma::vec C;
	arma::vec D;

    arma::vec zfactPart =
    {
        7.511255444649424828587030047762276930524e-1,
        5.311259660135984572385365242537567693773e-1,
        2.655629830067992286192682621268783846887e-1,
        1.08415633823009689789878853323749022452e-1,
        3.833071493144388678758035489700725876944e-2,
        1.212123635259875349071310579655202864027e-2,
        3.499099535541983943542762678345281363805e-3,
        9.351736874441379770202142628079336715494e-4,
        2.337934218610344942550535657019834178873e-4,
        5.510563799824823821067738395272340242989e-5,
        1.232199525075784976451417893101893685019e-5,
        2.627058214392003194960140445607747615225e-6,
        5.362460124872919207572643465049275268564e-7,
        1.051664954522700642243897278328369487056e-7,
        1.987459951592227331357006655358164809117e-8,
        3.628588825417294648844562498457222314549e-9,
        6.414499411475746158307919785557463603948e-10,
        1.100077573465546081464447454369726996765e-10,
        1.833462622442576802440745757282878327941e-11,
        2.97426912202769525933866781584586998244e-12,
        4.702732399958400033923267262424369697562e-13,
    };

	A = 1/sqrtl(this->bz_);
	//B = 1/sqrtl(pow(2, n_z)*sqrtl(arma::datum::pi)*Facto::factorial(n_z));
    B = zfactPart(n_z);
	C = arma::exp((-z%z)/(2*this->bz_*this->bz_));
	D = poly.hermite(n_z);
	return A*B*C%D;
}

// Basis r-function
arma::vec Basis::rPart(arma::vec r, int m, int n)
{
    Poly poly;

    poly.calcLaguerre(m, n, r%r / (br_*br_));

    double A = 1.0/br_*sqrtl(Facto::factorial(n)/(M_PI*Facto::factorial(n+abs(m))));
    arma::vec B = arma::exp((-0.5*r%r)/(this->br_*this->br_));
    arma::vec C = pow(r/br_,abs(m));
    arma::vec D = poly.laguerre(abs(m),n);
    return A*B%C%D;
}


arma::mat Basis::basisFunc(int m, int n, int n_z, arma::vec zVals, arma::vec rVals)
{       
  arma::mat f;
  f = rPart(rVals, m, n)* (zPart(zVals, n_z).t()); 
  return f;
}
