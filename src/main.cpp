#include "../headers/poly.h"
#include "../headers/basis.h"
#include "../headers/facto.h"
#include "../headers/density.h"
#include <math.h> 

#include <armadillo>
#include <iostream>
#include <fstream>



/**
 * écrit les valeurs de z sur la première ligne
 * puis les images de z par psi_n pour n allant de 0 à N
 * sur les lignes suivantes
 * avec le caractère ~ en début de chaque ligne
 * @param N le nombre des premières solutions +1 à dessiner
 * @param zVals les valeurs de z à évaluer
 * @oscillator un oscillateur bien initié
 
void writePlotData(int N, arma::rowvec zVals, Basis oscillator)
{
  ofstream file;
  arma::rowvec psi_n_z;

  file.open("python/plot_values.txt", ios::out);
  file << "~";
  zVals.print(file);

  for (int i = 0 ; i <= N ; i++)
  {
    psi_n_z = oscillator.analyticSol(i,zVals);
    file << "~";
    psi_n_z.print(file);
  }

  file.close();

}
*/
std::string cubeToDf3(const arma::cube &m)
{
  std::stringstream ss(std::stringstream::out | std::stringstream::binary);
  int nx = m.n_rows;
  int ny = m.n_cols;
  int nz = m.n_slices;
  ss.put(nx >> 8);
  ss.put(nx & 0xff);
  ss.put(ny >> 8);
  ss.put(ny & 0xff);
  ss.put(nz >> 8);
  ss.put(nz & 0xff);
  double theMin = 0.0;
  double theMax = m.max();
  for (uint k = 0; k < m.n_slices; k++)
  {
    for (uint j = 0; j < m.n_cols; j++)
    {
      for (uint i = 0; i < m.n_rows; i++)
      {
        uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
        ss.put(v);
      }
    }
  }
  return ss.str();
}



int main(){
  Density density;
  arma::vec y = arma::linspace( -10, 10,32 ) ;
  arma::vec r = arma::linspace( -10, 10,32 ) ;
  arma::vec z = arma::linspace( -10, 10,32 );
  arma::mat result,result1,result2,result3,result4,result5,result6;

  clock_t t1 = clock();
  result = density.direct_algorithm(z,r); 
  std::cout << "L'algorithe directe met "<<(clock() - t1) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms" <<  std::endl;
  clock_t t2 = clock();
  result1 = density.optim_algorithm1(z,r);  
  std::cout << "L'algorithe optimisation 1 met "<<(clock() - t2) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms " << std::endl;
  clock_t t3 = clock();
  result2 = density.optim_algorithm2(z,r);  
  std::cout << "L'algorithe optimisation 2 met "<<(clock() - t3) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms " << std::endl;
  clock_t t4 = clock();
  result3 = density.optim_algorithm3(z,r);  
  std::cout << "L'algorithe optimisation 3 met "<<(clock() - t4) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms  " <<  std::endl;
  clock_t t5 = clock();
  result4 = density.optim_algorithm4(z,r);  
  std::cout << "L'algorithe optimisation 4 met "<<(clock() - t5) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms  " <<  std::endl;
  clock_t t6 = clock();
  result5 = density.optim_algorithm5(z,r);  
  std::cout << "L'algorithe optimisation 5 met "<<(clock() - t6) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms " << std::endl;
  clock_t t7 = clock();
  result6 = density.optim_algorithm6(z,r);  
  std::cout << "L'algorithe optimisation 6 met "<<(clock() - t7) * 1.0 / CLOCKS_PER_SEC * 1000 << "ms  " << std::endl;
  arma::cube cube = density.coordonnesCylindriques(r, y, z, result);
  std::ofstream file;
  file.open("../df3/result3D.df3");
  file << cubeToDf3(cube);
  file.close();
  result.save("../python/plot2D.txt",arma::raw_ascii);


  
  return 0;
}