#include "../headers/basis.h"
#include "../headers/density.h"

Density::Density()
{
    basis = Basis(1.935801664793151,      2.829683956491218,     14,     1.3);
}

arma::mat Density::direct_algorithm(arma::vec zVals, arma::vec rVals){
	arma::mat rho;
	rho.load("rho.arma", arma::arma_ascii);

	int i = 0;
	int j = 0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); 

	for (int m = 0; m < basis.mMax; m++)
	{
  		for (int n = 0; n < basis.nMax(m); n++)
  		{
    			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    			{

      				for (int mp = 0; mp < basis.mMax; mp++)
      				{
        				for (int np = 0; np < basis.nMax(mp); np++)
        				{
          					for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
          					{	
            						arma::mat funcA = basis.basisFunc( m,  n,  n_z, zVals, rVals);
            						arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
            						result += funcA % funcB * rho(i,j); // mat += mat % mat * double
                        //std::cout << "Basis vector " << i << " " << j << ": m=" << mp << " n=" << np <<"   voila " <<funcA % funcB * rho(i,j)<< std::endl;
							        j++;
          					}
        				}
     				 }
                    j = 0;             
				    i++;	 
    			}
  		}
	}
	return result;
}



arma::mat Density::optim_algorithm1(arma::vec rVals, arma::vec zVals)
{

    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);
    int i = 0;
    int j = 0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); 
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat funcA = basis.basisFunc(m,  n,  n_z, zVals, rVals);
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
                            result += funcA % funcB * rho(i, j); // mat += mat % mat * double
                            
                            j++;
                        }
                    }
                }
                j = 0;
                i++;
            }            
        }
    }
    return result;
}


arma::mat Density::optim_algorithm2(arma::vec rVals, arma::vec zVals)
{

    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);

    int i = 0;
    int j = 0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); // number of points on r- and z- axes
    arma::imat sumN_zMax = arma::sum(basis.n_zMax, 1);

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    if (m != mp)
                    {
                        j += sumN_zMax(mp);
                        continue;
                    }
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat funcA = basis.basisFunc(m,  n,  n_z, zVals, rVals);
                            arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
                            result += funcA % funcB * rho(i, j); // mat += mat % mat * double
                            j++;
                        }
                    }
                }
                j = 0;
                i++;
            }            
        }
    }
    return result;
}

arma::mat Density::optim_algorithm3(arma::vec rVals, arma::vec zVals)
{

    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);

    int i = 0;
    int j = 0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); // number of points on r- and z- axes
    arma::imat sumN_zMax = arma::sum(basis.n_zMax, 1);
    
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat funcA = basis.basisFunc(m,  n,  n_z, zVals, rVals);
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    if (m != mp)
                    {
                        j += sumN_zMax(mp);
                        continue;
                    }
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {                            
                            arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
                            result += funcA % funcB * rho(i, j); // mat += mat % mat * double
                            j++;
                        }
                    }
                }
                j = 0;
                i++;
            }            
        }
    }
    return result;
}

arma::mat Density::optim_algorithm4(arma::vec rVals, arma::vec zVals)
{

    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);

    int i = 0;
    int j = 0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); // number of points on r- and z- axes
    arma::imat sumN_zMax = arma::sum(basis.n_zMax, 1);
    
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            arma::vec rPartA = basis.rPart(rVals, m, n);
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::vec zPartA = basis.zPart(zVals, n_z);
                arma::mat funcA = rPartA * zPartA.t();
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    if (m != mp)
                    {
                        j += sumN_zMax(mp);
                        continue;
                    }
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        arma::vec rPartB = basis.rPart(rVals, mp, np);
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {     
                            //j++;                       
                            arma::vec zPartB = basis.zPart(zVals, n_zp);
                            arma::mat funcB = rPartB * zPartB.t();
                            result += funcA % funcB * rho(i, j); // mat += mat % mat * double
                            j++;
                        }
                    }
                }
                j = 0;
                i++;
            }            
        }
    }
    return result;
}


arma::mat Density::optim_algorithm5(arma::vec rVals, arma::vec zVals)
{

    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);

    int i = 0;
    int j = 0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); // number of points on r- and z- axes
    arma::imat sumN_zMax = arma::sum(basis.n_zMax, 1);
    arma::mat sum = arma::zeros(rVals.n_rows, zVals.n_rows);
    
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            arma::vec rPartA = basis.rPart(rVals, m, n);
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::vec zPartA = basis.zPart(zVals, n_z);
                arma::mat funcA = rPartA * zPartA.t();
                sum = arma::zeros(rVals.n_rows, zVals.n_rows);
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    if (m != mp)
                    {
                        j += sumN_zMax(mp);
                        continue;
                    }
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        arma::vec rPartB = basis.rPart(rVals, mp, np);
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {                          
                            arma::vec zPartB = basis.zPart(zVals, n_zp);
                            arma::mat funcB = rPartB * zPartB.t();
                            if (n_z > n_zp)
                            {
                                sum += 2 * funcB * rho(i,j);
                            }
                            else if (n_z == n_zp)
                            {
                                sum += funcB * rho(i,j);
                            }
                            j++;
                        }
                    }
                }
                result += funcA % sum;
                j = 0;
                i++;
            }            
        }
    }
    return result;
}


// Deletion of the mp loop because when ma != mb the rho(a,b) = 0
arma::mat Density::optim_algorithm6(arma::vec rVals, arma::vec zVals)
{

    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);

    int i = 0;
    int j = 0;
    int old_j=0;
    arma::mat result = arma::zeros(rVals.n_rows, zVals.n_rows); // number of points on r- and z- axes
    arma::imat sumN_zMax = arma::sum(basis.n_zMax, 1);
    arma::mat sum = arma::zeros(rVals.n_rows, zVals.n_rows);
    
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            arma::vec rPartA = basis.rPart(rVals, m, n);
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::vec zPartA = basis.zPart(zVals, n_z);
                arma::mat funcA = rPartA * zPartA.t();
                sum = arma::zeros(rVals.n_rows, zVals.n_rows);
                j = old_j;
                for (int np = 0; np < basis.nMax(m); np++)
                {
                    arma::vec rPartB = basis.rPart(rVals, m, np);
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {                          
                        arma::vec zPartB = basis.zPart(zVals, n_zp);
                        arma::mat funcB = rPartB * zPartB.t();
                        if (n_z > n_zp)
                        {
                            sum += 2 * funcB * rho(i,j); 
                        }
                        else if (n_z == n_zp)
                        {
                            sum += funcB * rho(i,j); 
                        }
                        j++;
                    }
                }
                result += funcA % sum;
                i++;
            }            
        }
        old_j=i;
    }
    return result;
}

arma::cube Density::coordonnesCylindriques(arma::vec R, arma::vec Y,arma::vec Z, arma::mat result)
{

    arma::cube rho3d(R.n_elem, Y.n_elem, Z.n_elem);
    for (uint i = 0; i < R.n_elem; i++)
    {
        for (uint j = 0; j < Y.n_elem; j++)
        {
            double r = sqrt(R(i)*R(i) + Y(i)*Y(i));
            int ind = 0;
            for (uint k = 1; k < R.n_elem; k++)
            {
                if (abs(r - R[k]) < abs(r - R[ind]))
                {
                    ind = k;
                }
            }
            for (uint k = 0; k < Z.n_elem; k++)
            {
                rho3d(i,j,k) = result(ind, k);
            }
        }
    }

    return rho3d;

}