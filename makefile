all: src test

src:
	$(MAKE) -C src
test:
	$(MAKE) -C tests

.PHONY: clean src test

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean
