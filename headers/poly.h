#ifndef POLY_H
#define POLY_H

#include <armadillo>


class Poly
{


    private: 

    arma::mat H;

    arma::cube L;


    public:
    
    Poly();
    virtual ~Poly();

    /**
     * DEPRECATED
    *@param n le degré du polynôme à évaluer
    *@param zVals un vecteur ligne représentant les z à évaluer
    *@return un vecteur ligne de taille égale à celle de zVals avec les valeurs correspondantes
    */
    static arma::rowvec evalHermite(int, arma::rowvec);
    
    /**
     * Calcule les images par les premiers polynômes d'Hermite
     * des valeurs de z fournies, et les met en cache dans l'attribut privé H
     * Lève l'exception invalid_argument si N n'est pas strictement positif
     * @param N le nombre des premiers polynomes à calculer
     * @param zVals les valeurs de z à évaluer
     */
    void calcHermite(int N, arma::vec zVals);

    /**
     * Calcule les images par les premiers polynômes de Laguerre (m,n)
     * des valeurs de z fournies, et les met en cache dans l'attribut privé L
     * Lève l'exception invalid_argument si N n'est pas strictement positif
     * @param m paramètre du polynôme
     * @param N le nombre des premiers polynomes à calculer
     * @param zVals les valeurs de z à évaluer
     */
    void calcLaguerre(int m, int N, arma::vec zVals);

    /**
     * Renvoie le n-ème polynôme d'Hermite, évalué aux valeurs fournis dans le dernier appel de calcHermite
     * @requires calcHermite doit avoir été appelé au préalable.
     */
    arma::vec hermite(int n);

    /**
     * Renvoie le n-ème polynôme de Laguerre de paramètre m, évalué aux valeurs fournis dans le dernier appel de calcHermite
     * @requires calcLaguerre doit avoir été appelé au préalable.
     */
    arma::vec laguerre(int m, int n);

};

#endif // POLY_H
