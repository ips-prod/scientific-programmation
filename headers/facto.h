#ifndef FACTO_H
#define FACTO_H

class Facto
{
    public:

    Facto();
    
    /**
    *@param n le nombre dont on veut la factorielle
    *@return la factorielle de n
    */
    static int factorial(int);
};

#endif // FACTO_H