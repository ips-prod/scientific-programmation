#ifndef DENSITY_H
#define DENSITY_H


#include "basis.h"


class Density
{
	public:

	  Density();
	  arma::mat direct_algorithm(arma::vec , arma::vec );
	  arma::mat optim_algorithm1(arma::vec , arma::vec );
	  arma::mat optim_algorithm2(arma::vec , arma::vec );
	  arma::mat optim_algorithm3(arma::vec , arma::vec );
	  arma::mat optim_algorithm4(arma::vec , arma::vec );
	  arma::mat optim_algorithm5(arma::vec , arma::vec );
	  arma::mat optim_algorithm6(arma::vec , arma::vec );
	  arma::cube coordonnesCylindriques(arma::vec , arma::vec ,arma::vec , arma::mat );
	private:
   	  Basis basis;

};

#endif
