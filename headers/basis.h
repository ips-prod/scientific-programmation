#ifndef BASIS_H
#define BASIS_H

#include <iostream>
#include <armadillo>
#include "../headers/facto.h"
#include "../headers/poly.h"


class Basis
{
    public:
        double br_;
        double bz_;
        double Q_;
        int N_;
        int mMax;
        arma::ivec nMax;
        arma::imat n_zMax;



    public:
        Basis(double br, double bz, int N,double Q); //constructeur
        Basis();
        arma::vec zPart(arma::vec z, int n_z);
        arma::vec rPart(arma::vec , int , int );
        double calc_nzMax(int N, double Q, int i);
        arma::mat basisFunc(int, int, int ,arma::vec, arma::vec);

};


#endif // BASIS_H
