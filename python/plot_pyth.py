#!/usr/local/bin/python
# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
f = open('plot2D.txt') #open the file 
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(np.loadtxt(f, dtype=np.float64), interpolation='bilinear', cmap="Spectral_r")
fig.colorbar(cax)
plt.title("Densité nucléaire (pour une tranche theta=0)");
plt.xlabel("z (de -10 fm à 10 fm)")
plt.ylabel("x (de -10 fm à 10 fm)")
plt.show()



