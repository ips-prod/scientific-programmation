import matplotlib.pyplot as plt
f = open('plot_values.txt') #open the file 
lignes=[l1.strip().split() for l1 in f.readlines()] # liste des donnees a tracer 
for i in range(1,len(lignes)):

	plt.plot(lignes[0],lignes[i])

plt.title('analytic solutions of the 1D quantum harmonic oscillator')
plt.ylabel(r'$\psi_n(z)$')
plt.xlabel('z')
plt.show()
